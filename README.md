# Theme carousel

Cycle a predefined list of themes in a carousel fashion.

# Install

With use-package:

```emacs-lisp
(use-package theme-carousel
  :load-path "~/.emacs.d/custom/theme-carousel/theme-carousel.el" ; Set to the path to the file
  :bind (("C-c C-<right>" . theme-carousel-next) ; example bindings
         ("C-c C-<left>" . theme-carousel-previous))
  :config
  (setq theme-carousel-list 
        '(doom-iosvkem doom-moonlight doom-ir-black doom-acario-dark)) ; Just an example, use themes you like
  (setq theme-carousel-save-file "~/.emacs.d/etc/theme-carousel/save.txt") ; this is the default value
  (theme-carousel-load-selected)) ; Load the selected theme on start up
```
