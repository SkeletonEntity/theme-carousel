;;; theme-carousel.el --- Cycle a predefined list of themes in a carousel fashion.                    -*- lexical-binding: t; -*-

;; Copyright (C) 2021 SkeletonAdventure

;; Author: SkeletonAdventure <skeletonadventure@protonmail.com>
;; Keywords: theme, carousel, cycle, cycle themes, theme carousel
;; Version: 1.0

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; with version 2
;; of the License, and no other version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <https://www.gnu.org/licenses

;;; Commentary:

;; Cycle a predefined list of themes in a carousel fashion.

;;; Code:

(defgroup theme-carousel nil
  "Cycle a predefined list of themes in a carousel fashion."
  :group 'tools)

(defcustom theme-carousel-list nil
  "The list of themes to cycle through."
  :type 'sequence
  :group 'theme-carousel)

(defcustom theme-carousel-save-file "~/.emacs.d/var/theme-carousel/save.txt"
  "The index in the theme-list for the currently loaded theme"
  :type 'string
  :group 'theme-carousel)

(defadvice load-theme (before theme-dont-propagate activate)
  "Ensure the loaded theme is disabled before loading a new one."
  (mapc #'disable-theme custom-enabled-themes))

(defun theme-carousel-save-index (n)
  (if (not (file-exists-p theme-carousel-save-file))
      (make-empty-file theme-carousel-save-file))
  (write-region (format "%d" n) nil theme-carousel-save-file))

(defun theme-carousel-load-index ()
  (if (file-exists-p theme-carousel-save-file)
      (string-to-number
       (with-temp-buffer (insert-file-contents theme-carousel-save-file) (buffer-string)))
    0))

(defun theme-carousel-move (n)
  "Goes n indices from the currently loaded theme in the theme-carousel-list"
  (if (not theme-carousel-list)
      (message "You have no themes in your theme-carousel-list.")
    (let ((len (length theme-carousel-list)))
      (if (> len 1)
	  (theme-carousel-save-index (% (+ (theme-carousel-load-index) n len) len)))
      (theme-carousel-load-selected))))

(defun theme-carousel-next ()
  "Goes to the next theme in the carousel."
  (interactive)
  (theme-carousel-move 1))

(defun theme-carousel-previous ()
  "Goes to the previous theme in the carousel."
  (interactive)
  (theme-carousel-move -1))

(defun theme-carousel-load-selected ()
  "Loads the theme the carousel index points to."
  (interactive)
  (let ((theme (nth (theme-carousel-load-index) theme-carousel-list)))
    (load-theme theme t)
    (message "Loaded theme: %s" theme)))

(provide 'theme-carousel)

;;; theme-carousel.el ends here
